const express = require("express");
const webpush = require("web-push");
const bodyParser = require("body-parser");
const path = require("path");


const app = express();

app.use(express.static(path.join(__dirname, "client")));

app.use(bodyParser.json());

const publicVapidKey = 'BC6R_PMUzKVXif1cna6HcpL29Dzb8M_FVBt2SkbLr35Ny6N3Pfo7ySizvRL8d60GRd8k-PE4JaaHH5lI_s6BqRs';
const privateVapidKey = '_nnk-2G19f0xACKxgr4XIq4Amwm5KmKi_3JTk_JMbF8';

webpush.setVapidDetails(
    "mailto:test@test.com",
    publicVapidKey,
    privateVapidKey
  );

  var subscription;
//Route for subscribe
app.post("/subscribe", (req, res) => {
  // Get pushSubscription object
  subscription = req.body;
  console.log('subscribed', subscription);

  // Send 201 - resource created
  res.status(201).json({});
});

app.get('/ping', (req, res) => {
res.status(200).json({});
  const payload = JSON.stringify({ title: "Push Notifiy" });

  // Pass object into sendNotification
  webpush
    .sendNotification(subscription, payload)
    .catch(err => console.error(err));
})

const port = 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));
