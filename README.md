# Steps
1. Run `yarn install` and `yarn start`
2. Open `http://localhost:5000` in your browser and grant it permission for notification.
3. Access `http://localhost:5000` for triggering notification.

# Generating VAPID keys
    ./node_modules/.bin/web-push generate-vapid-keys

# Sending notification to Android devices
On Android Google Chrome, only sites using HTTPS can ask for notification permission.
You can use "localtunnel" to setup HTTPS in your development environment easily.  
Reference: https://github.com/localtunnel/localtunnel
